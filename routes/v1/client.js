const express = require('express');
const Clients = require('../../libs/Database/Models/Client');
const log = require('../../libs/log')(module);
const router = express.Router();



router.post('/', function (req, res, next) {
    let client = new Clients();
    let clientApp = client.Model()
    const users = new clientApp({
        name: req.body.name,
        clientId: req.body.clientId,
        clientSecret: req.body.clientSecret,
        mobile:req.body.mobile
    });
    users.save(function (err) {
        if (!err) {

            res.send({status: 'OK', users: users});
        } else {
            log.error(`create client ${err}`);

            if (err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({error: 'Validation error'});
                // logger.error("Validation error");
            }else if (err.code == 11000) {
                res.statusCode = 400;
                res.send({error: 'duplicate key', msg: err.errmsg});
                // logger.error("duplicate key");
            } else {
                res.statusCode = 500;
                res.send({error: 'Server error'});
                // logger.error(JSON.stringify(err.stack));
            }
        }
    });
});


module.exports = router;