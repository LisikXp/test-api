const express = require('express');
const router = express.Router();
const User = require('../../libs/Database/Models/User');
const log = require('../../libs/log')(module);

/* GET users listing. */
router.post('/', async function (req, res, next) {
    let user = new User();
    let UserModel = user.Model();
    const users = new UserModel({
        creationdatetime: new Date(),
        password: req.body.password,
        displayname: req.body.displayname,
        email: req.body.email,
    });
    try {
        const _save = await users.save();
        res.json({
            _id:_save._id,
            displayname:_save.displayname,
            email:_save.email
        });
    } catch (err) {
        log.error(`registration user: ${err}`);

        if (err.name == 'ValidationError') {
            res.statusCode = 400;
            res.send({error: 'Validation error', message: err.message});
        } else if (err.code == 11000) {
            res.statusCode = 400;
            res.send({error: 'duplicate key', msg: err.errmsg});
        } else {
            res.statusCode = 500;
            res.send({error: 'Server error'});
        }
    }
});

module.exports = router;
