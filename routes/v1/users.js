const express = require('express');
const router = express.Router();
const passport = require('passport');
const User = require('../../libs/Database/Models/User');
const log = require('../../libs/log')(module);

/* GET users listing. */
router.get('/:id', passport.authenticate('bearer', {session: false}), async function (req, res, next) {
    const _uid = Mongoose.Types.ObjectId(req.params.id);
    let user = new User();
    let UserModel = user.Model();
    try{
        const getUser = await UserModel.findById(_uid).select(['displayname','email']).lean().exec();
        if(getUser !== null){
            res.json(getUser);
        }else{
            return res.status(404).send({error: 'Not found'});
        }
    }catch (e) {
        log.error(`get profile: ${e}`);
        return res.status(500).send({error: 'error'});
    }

});
router.put('/:id', passport.authenticate('bearer', {session: false}), function (req, res, next) {
    const _uid = Mongoose.Types.ObjectId(req.params.id);
    let user = new User();
    let UserModel = user.Model();
    return UserModel.findById(req.params.id, async function (err, usermodel) {
        if (err) {
            log.error(`edit profile: ${err}`);
            res.status(500).send({error: 'error'});
        }
        if (typeof (req.body.displayname) != "undefined" && req.body.displayname !== null && req.body.displayname !== 'null') {
            usermodel.displayname = req.body.displayname;
        }

        if (typeof (req.body.email) != "undefined" && req.body.email !== null && req.body.email !== 'null') {
            usermodel.email = req.body.email;
        }
        if (typeof (req.body.password) != "undefined" && req.body.password !== null && req.body.password !== 'null') {
            usermodel.password = req.body.password;
        }
        return usermodel.save(async function (err) {
            if (!err) {

                res.json({
                    _id:usermodel._id,
                    displayname:usermodel.displayname,
                    email:usermodel.email
                });
            } else {
                log.error(`edit profile save: ${err}`);

                if (err.name == 'ValidationError') {
                    res.statusCode = 400;
                    res.send({error: 'Validation error', message: err.message});
                    // logger.error(`Validation error user id: ${req.params.id}, phone: ${usermodel.mobilephonenumber}, error: ${err.message}`);
                } else if (err.code == 11000) {
                    res.statusCode = 400;
                    res.send({error: 'duplicate key', msg: err.errmsg});
                    // logger.error(`duplicate key: user id: ${req.params.id}, phone: ${usermodel.mobilephonenumber}, error: ${JSON.stringify(err.stack)}`);
                } else {
                    res.statusCode = 500;
                    res.send({error: 'Server error'});
                    // logger.error(`Server error user id: ${req.params.id}`);
                }
            }

        });
    });
});
router.delete('/:id', passport.authenticate('bearer', {session: false}), function (req, res, next) {
    const _uid = Mongoose.Types.ObjectId(req.params.id);
    let user = new User();
    let UserModel = user.Model();
    return UserModel.findById(_uid, function (err, usermodel) {
        if (!usermodel) {
            res.statusCode = 404;
            return res.send({error: 'Not found'});
        }

        return usermodel.remove(function (err) {
            if (!err) {
                return res.send({status: 'OK'});

            } else {
                log.error(`delete profile: ${err}`);
                res.statusCode = 500;
                return res.send({error: 'Server error (User removes)'});
            }
        });

    });
});

module.exports = router;
