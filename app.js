const express = require('express');
const path = require('path');
const fs = require('fs');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const createError = require('http-errors');
const logger = require('morgan');
const cors = require('cors');
const helmet = require('helmet');
const passport = require('passport');

const indexRouter = require('./routes/index');
const setClientRouter = require('./routes/v1/client');
const usersRouter = require('./routes/v1/users');
const loginRouter = require('./routes/v1/login');
const registrationRoute = require('./routes/v1/registration');

const app = express();
app.use(cors());
require('./libs/auth/auth');
const oauth2 = require('./libs/auth/oauth2');
//process.env.NODE_ENV = 'production'
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
const sixtyDaysInSeconds = 5184000
app.use(helmet.hsts({
    maxAge: sixtyDaysInSeconds
}));
if(process.env.NODE_ENV==="prod") {

    const accessLogStream = fs.createWriteStream(__dirname + '/logs/' + "access.log", {flags: 'a'});
    app.use(logger('combined',{stream: accessLogStream}));
}
else {
    app.use(logger("dev")); //log to console on development
}
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

app.use('/', indexRouter);
app.use('/api/v1/client', setClientRouter);
app.use('/api/v1/login', loginRouter);
app.use('/api/v1/user', usersRouter);
app.use('/api/v1/registration', registrationRoute);

app.use(function (req, res, next) {
    next(createError(404));
});
module.exports = app;
