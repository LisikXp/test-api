const winston = require('winston');
const setlogger = (module) => {
    const path = module.filename.split('/').slice(-2).join('/');
    let foldier = __dirname.split('/');
    foldier.pop();
    let Full_Path = `${foldier.join('/')}/` || `${process.cwd()}/`;
    let date = new Date();
    let mounth = date.getMonth() + 1

    let day = date.getDate();
    let namedir = `${date.getFullYear()}-${mounth < 10 ? '0' + mounth : mounth}-${day < 10 ? '0' + day : day}`;
    let namefile = `${Full_Path}logs/${namedir}.log`;

    let _transports = [
        new winston.transports.Console({
            colorize: true,
            level: 'debug',
            label: path
        })]
    if (process.env.NODE_ENV === "prod") {
        _transports = [ new winston.transports.File({filename: namefile})]
    }

    return winston.createLogger({
        transports: _transports
    });

}
module.exports = setlogger;