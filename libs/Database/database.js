'use strict'
let mongoose = require("mongoose")//.set('debug', true)
global.Mongoose = mongoose
mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', true);
const log = require('../log')(module);

const {config} = require('../../config');
mongoose.Promise = global.Promise;

class Database {

    constructor() {
        let self = this
        this.dbname = config.use_db.name;
        this.url = `${config.use_db.ip}:${config.use_db.port}`;
        this.user = config.use_db.user;
        this.pass = config.use_db.pass;
        // this.connection(),
        this.mongoose = mongoose
        this.Schema = mongoose.Schema
        this.db = mongoose.connection
    }

     connection() {
        //  console.log(process.env);
        try {
            const options = {
                useNewUrlParser: true,
                // autoReconnect: true,
                // reconnectInterval: 1000, // Reconnect every 500ms
                poolSize: 2, // Maintain up to 10 socket connections
                // If not connected, return errors immediately rather than waiting for reconnect
                keepAlive: true,
                bufferMaxEntries: 0,
                connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
                socketTimeoutMS: 60000, // Close sockets after 45 seconds of inactivity
                useCreateIndex: true,
                // auth:{authdb:"admin"},  //старый  18.217.255.103
                auth: {authSource: "admin"}, //новый  3.121.53.116
            };
            mongoose.connect(`mongodb://${this.user}:${this.pass}@${this.url}/${this.dbname}`, options, function (err) {//?authSource=admin
                // mongoose.connect(`mongodb://${this.url}/${this.dbname}`, options, function (err) {//?authSource=admin

                if (err) {
                    throw err
                    log.error('connection error: ', JSON.stringify(err.stack));
                }
            });

            this.db.on('error', function (err) {
                log.error('connection error:', err.message);

            });
            this.db.on('disconnected', function callback() {
                log.error('connection error');

            });
            this.db.on('disconnecting', function () {
                // logger.error('mongodb is disconnecting!!!');
                log.error('mongodb is disconnecting');
            });

            this.db.on('reconnected', function () {
                log.error('mongodb is reconnected!!!');
            });

            this.db.on('timeout', function (e) {
                log.error("db: mongodb timeout " + e);
            });

            this.db.on('close', function () {
                log.error("mongodb connection closed");
            });

            this.db.once('open', function callback() {
                console.log("Connected to DB!");
            });
        } catch (e) {
            log.error(`mongodb connection closed ===> ${e}`);
        }
    }
}

module.exports = Database
