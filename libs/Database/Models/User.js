'use strict'

const Database = require('../database')
const CryptoJS = require("crypto-js");
class User extends Database {

    constructor() {
        super()
        //console.log('User')
    }

    Model() {

        let SchemaTypes = this.mongoose.Schema.Types;
        let User = new this.Schema({
            isremoved: {type: Boolean, default: false},
            creationdatetime: {type: Date, default: new Date()},
            lastchangedatetime: {type: Date, default: ''},
            hashedPassword: {
                type: String,
                required: true
            },
            passwordsalt: {type: String},
            displayname: {type: String},
            email: {
                type: String,
                unique: true,
                required: true
            },
        });
        User.index({creationdatetime : -1})
        User.index({creationdatetime : 1})
        User.virtual('userId')
            .get(function () {
                return this._id;
            });
        User.methods.encryptPassword = function (password) {
            return CryptoJS.PBKDF2(password, this.passwordsalt, {
                keySize: 512/32,
                iterations: 10000
            }).toString(CryptoJS.enc.Hex);
        };

        User.virtual('password')
            .set(function (password) {
                this._plainPassword = password;
                this.passwordsalt = CryptoJS.lib.WordArray.random(128/8).toString(CryptoJS.enc.Hex);
                this.hashedPassword = this.encryptPassword(password);
            })
            .get(function () {
                return this._plainPassword;
            });


        let self = this
        User.methods.checkPassword = async function (password) {
            /*
            * Проверяем пароль пользователя
            * */
            return this.encryptPassword(password) === this.hashedPassword;
        };

        User.path('email').validate(async (email)=> {
            let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            // return emailRegex.test(email); // Assuming email has a text attribute
            // const a = await this.getChekUserEmail(email)
            // if (emailRegex.test(email) === true && a === true){
            if (emailRegex.test(email) === true){
                return true;
            }else{
                return false;
            }
        })

        User.path('displayname').validate(function (v) {
            return v.length >= 2 && v.length < 70;
        });
        try {
            return this.mongoose.model('User', User);
        } catch (error) {
            return this.mongoose.model('User');
        }

    }

    async getSetting() {
        let settings = new Settings()
        let setting = settings.Model()
        let a = null
        await setting.findOne().exec().then(async setting => {
            a = await setting
        }).catch(async err => {
            await console.log(err)
        });
        return await a
    }

    getChekUserEmail(val) {
        return new Promise((resolve, reject) => {
            let User = this.Model();
            User.findOne({
                AdditionalEmail: val
            }).exec().then(async user => {
                if (user === null) {
                    resolve(true);
                } else {
                    if (user.AdditionalEmail){
                        let find = user.AdditionalEmail.find(item=>item===val)
                        find !== undefined ? resolve(true):resolve(false);
                    }
                    resolve(false);
                }
            }).catch(err => {
                console.log(err);
            });
        })
    }

    getChekUser(val) {
        return new Promise((resolve, reject) => {
            let User = this.Model();
            User.findOne({
                AdditionalMobilePhone: val
            }).exec().then(async user => {
                if (user === null) {
                    resolve(true);
                } else {
                  if (user.AdditionalMobilePhone){
                      let find = user.AdditionalMobilePhone.find(item=>item===val)
                      find !== undefined ? resolve(true):resolve(false);
                  }
                    resolve(false);
                }
            }).catch(err => {
                console.log(err);
            });
        })
    }


}

module.exports = User
