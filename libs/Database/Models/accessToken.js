
'use strict'
const Database = require('../database')

class AccessToken extends Database{

    constructor() {
        super()
         //console.log('AccessToken')
    }
    Model(){
        var SchemaTypes = this.mongoose.Schema.Types;
        let AccessToken = new this.Schema({
            userId: {
                type: String,
                required: true
            },

            clientId: {
                type: String,
                required: true
            },

            token: {
                type: String,
                unique: true,
                required: true
            },

            created: {
                type: Date,
                default: Date.now
            },
            mobile:{
                type:Boolean,
                default:false
            }

        });
        try {
            return this.mongoose.model('AccessToken', AccessToken);;
        } catch (error) {
            return this.mongoose.model('AccessToken');
        }

    }


}
module.exports = AccessToken