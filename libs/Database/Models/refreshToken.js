
'use strict'

const Database = require('../database')

class RefreshToken extends Database{

    constructor() {
        super()
        //console.log('RefreshToken')
    }
    Model(){
        const SchemaTypes = this.mongoose.Schema.Types;
        let RefreshToken = new this.Schema({
            userId: {
                type: String,
                required: true
            },
            clientId: {
                type: String,
                required: true
            },
            token: {
                type: String,
                unique: true,
                required: true
            },
            created: {
                type: Date,
                default: Date.now
            },
            mobile:{
                type:Boolean,
                default:false
            }

        });
        try {
            return this.mongoose.model('RefreshToken', RefreshToken);
        } catch (error) {
            return this.mongoose.model('RefreshToken');
        }

    }


}
module.exports = RefreshToken