'use strict'

const Database = require('../database')

class Clients extends Database{

    constructor() {
        super()
        //console.log('Clients')
    }
    Model(){
        var SchemaTypes = this.mongoose.Schema.Types;
        let Client = new this.Schema({
            name: {
                type: String,
                unique: true,
                required: true
            },
            clientId: {
                type: String,
                unique: true,
                required: true
            },
            clientSecret: {
                type: String,
                required: true
            },
            mobile:{
                type:Boolean,
                default:false
            }
        });
        try {
            return this.mongoose.model('Client', Client);
        } catch (error) {
            return this.mongoose.model('Client');
        }

    }


}
module.exports = Clients