const passport = require('passport');
const BasicStrategy = require('passport-http').BasicStrategy;
const ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;
const BearerStrategy = require('passport-http-bearer').Strategy;
const {config} = require('../../config');
const log = require('../log')(module);
const User = require('../Database/Models/User');
const Clients = require('../Database/Models/Client');
const AccessToken = require('../Database/Models/accessToken');
const RefreshToken = require('../Database/Models/refreshToken');


let user = new User();
let UserModel = user.Model();

let client = new Clients();
let clientModel = client.Model();

let AToken = new AccessToken();
let acctoken = AToken.Model();

let RToken = new RefreshToken();
let reftoken = RToken.Model();

// Bearer Token strategy
// https://tools.ietf.org/html/rfc6750
passport.use(new BasicStrategy(
    function (username, password, done) {

        clientModel.findOne({clientId: username}, function (err, client) {
            if (err) {
                // logger.error("BasicStrategy error " + JSON.stringify(err.stack));
                log.error(`BasicStrategy error: ${err}`);
                return done(err);
            }

            if (!client) {
                return done(null, false);
            }

            if (client.clientSecret !== password) {
                return done(null, false);
            }

            return done(null, client);
        });
    }
));
// Client Password - credentials in the request body
passport.use(new ClientPasswordStrategy(
    function (clientId, clientSecret, done) {

        clientModel.findOne({clientId: clientId}, function (err, client) {
            if (err) {
                log.error(`ClientPasswordStrategy error: ${err}`);
                return done(err);
            }

            if (!client) {
                return done(null, false);
            }

            if (client.clientSecret !== clientSecret) {
                return done(null, false);
            }

            return done(null, client);
        });
    }
));
passport.use(new BearerStrategy(
    function (accessToken, done) {

        try {
            if (accessToken == '' || accessToken === null) {
                return done(null, false);
            }
            acctoken.findOne({token: accessToken}, function (err, token) {
                if (err) {
                    log.error(`acctoken error: ${err}`);
                    return done(err);
                }
                if (token === null) {
                    return done(null, false);
                }
                // console.log('token', token.mobile, Date.now(), token.created,new Date(token.created).getTime(),  Math.round((Date.now() - new Date(token.created).getTime()) / 1000), config.get('security:tokenLife'));
                if (Math.round((Date.now() - new Date(token.created).getTime()) / 1000) > config.tokenLife) {
                    acctoken.findOneAndRemove({token: accessToken}, function (err) {
                        if (err) {
                            log.error(`acctoken.findOneAndRemove ERROR: ${err}`);

                            return done(null, false);
                        }
                    });
                    return done(null, false, {message: 'Token expired'});
                }
                UserModel.findById(token.userId, function (err, user) {

                    if (err) {
                        // logger.error(`UserModel catch ERROR: ${err.stack}`);
                        log.error(`UserModel ERROR: ${err}`);
                        return done(null, false);
                    }

                    if (user === null) {
                        return done(null, false, {message: 'Unknown user'});
                    }

                    let info = {scope: '*'};
                    done(null, user, info);

                });
            });
        } catch (e) {
            log.error(`BearerStrategy catch ERROR: ${e}`);
            return done(null, false);
        }

    }
));
