const oauth2orize = require('oauth2orize');
const passport = require('passport');
const CryptoJS = require("crypto-js");
const log = require('../log')(module);
const {config} = require('../../config');
const User = require('../Database/Models/User');
const AccessToken = require('../Database/Models/accessToken');
const RefreshToken = require('../Database/Models/refreshToken');

let user = new User();
let UserModel = user.Model()

let AToken = new AccessToken();
let acctoken = AToken.Model()

let RToken = new RefreshToken();
let reftoken = RToken.Model()

// Create OAuth 2.0 server
const aserver = oauth2orize.createServer();

// Generic error handler
const errFn = function (cb, err) {
    if (err) {
        log.error(`error handler: ${err}`);
        return cb(err);
    }
};

// Destroy any old tokens and generates a new access and refresh token
const generateTokens = async function (data, done) {


    // Curries in `done` callback so we don't need to pass it
    let errorHandler = errFn.bind(undefined, done),
        refreshToken,
        refreshTokenValue,
        token,
        tokenValue;

    /*reftoken.remove(data, errorHandler);
    acctoken.remove(data, errorHandler);*/

    tokenValue = CryptoJS.lib.WordArray.random(64).toString(CryptoJS.enc.Hex);
    refreshTokenValue = CryptoJS.lib.WordArray.random(64).toString(CryptoJS.enc.Hex);
    removeOldTokens(data.userId, data.mobile, done, tokenValue, refreshTokenValue).then(resp => {
        data.token = tokenValue;
        token = new acctoken(data);


        token.save(function (err) {
            if (err) {
                log.error(`delete old token: ${err}`);
                return done(err);
            }

            data.token = refreshTokenValue;
            refreshToken = new reftoken(data);
            refreshToken.save(errorHandler);
            done(null, tokenValue, refreshTokenValue, {
                'expires_in': config.tokenLife
            });
        });
    })
};


const removeOldTokens = async (uid, mobile, done, token, refresh) => {
    return acctoken.findOneAndRemove({
        $and: [
            {userId: uid},
            {token: {$ne: token}}
        ]
    }, async function (err) {
        if (err) {
            log.error(`removeOldTokens: ${err}`);
            return done(err);
        }
        await reftoken.findOneAndRemove({
            $and: [
                {userId: uid},
                {token: {$ne: refresh}}
            ]
        }, function (err) {
            if (err) {
                return done(err);
            }
        });
        return true;
    });
}

// Exchange username & password for access token
aserver.exchange(oauth2orize.exchange.password(function (client, username, password, scope, done) {
    // var query = {$or: [{login: username}, {email: username}, {mobilephonenumber: username}]};
    const query = {email: username};

    UserModel.findOne(query, async function (err, user) {
        // console.log(user);
        if (user) {
            if (err) {
                log.error(`search user: ${err}`);

                return done(err);
            }
            let pass = await user.checkPassword(password);
            if (!user || !pass) {
                return await done(null, false);
            }


            const model = {
                created: new Date(),
                userId: user.userId,
                clientId: client.clientId
            };
            await generateTokens(model, done);
        } else {
            return done(null, false, {message: 'Unknown user'});
        }


    });

}));

// Exchange refreshToken for access token
aserver.exchange(oauth2orize.exchange.refreshToken(function (client, refreshToken, scope, done) {

    reftoken.findOne({token: refreshToken, clientId: client.clientId}, function (err, token) {
        if (err) {
            return done(err);
        }

        if (!token) {
            return done(null, false);
        }

        UserModel.findById(token.userId, async function (err, user) {
            if (err) {
                log.error(`refreshToken: ${err}`);

                return done(err);
            }
            if (!user) {
                return done(null, false);
            }

            const model = {
                created: new Date(),
                userId: user.userId,
                clientId: client.clientId
            };

        });
    });
}));

// token endpoint
//
// `token` middleware handles client requests to exchange authorization grants
// for access tokens. Based on the grant type being exchanged, the above
// exchange middleware will be invoked to handle the request. Clients must
// authenticate when making requests to this endpoint.

module.exports.token = [
    passport.authenticate(['basic', 'oauth2-client-password'], {session: false}),
    aserver.token(),
    aserver.errorHandler()
];