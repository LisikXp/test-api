## Hi everyone
#### For install all modules run npm i in your console
#### In config.js write your DB data for connection
### Run project in dev mode  - npm run start:dev;
### or  production mode - npm run start:prod
#### To create tokens you need to create client app with /api/v1/client route:
- name - name your app,
- clientId - random hash,
- clientSecret -  random hash
#### To Generate token for work with API - /api/v1/login
- "grant_type":"password",
- "client_id": clientId from your app,
- "client_secret": clientSecret from your app,
- "username": your emain,
- "password": your password

### I include in repo this file API.postman_collection.json for your postman app